const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // Initialization
        // create objects... etc...
        console.log("Initialising tests");

    })
    // testing teh function 'add'
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3 for some reason..");
    });
    // testing the function 'multiply'
    it("Can multiply 2 and 3 together", () => {
        expect(mylib.multiply(2,3)).equal(6, "2 * 3 is not 6 for some reason..");
    });
    // testing the function 'subtract'
    it("Can subtract 2 from 3 ", () => {
        expect(mylib.subtract(3,2)).equal(1, "3 - 2 is not 1 for some reason..");
    });
    // testing the function 'divide'
    it("Can divide 6 by 2 ", () => {
        expect(mylib.divide(6,2)).equal(3, "6 / 2 is not 3 for some reason..");
    });
    // testing if an error is thrown when dividing by 0. Does not work.
    it("Throws error when dividing by 0", () => {
        expect(mylib.divide(3,0)).toThrow(new Error, "Cannot divide by 0");
    });




    after(() => {
        // Cleanup
        // For example: shutdown the Express server.
        console.log("Testing completed!");
    });
});